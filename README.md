
# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [Primeiros Computadores]()
1. [Evolução dos Computadores Pessoais e sua Interconexão]()
    - [Primeira Geração]()
1. [Computação Móvel]()
1. [Futuro]()




## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://secure.gravatar.com/avatar/50a0881c9275b9107c0bfb59b8f9326e?s=800&d=identicon)  | Eduardo Rocha | [EduRochaa](https://gitlab.com/EduRochaa) | [eduardorocha.2017@alunos.utfpr.edu.br](mailto:eduardorocha.2017@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168722/avatar.png?width=400)  | João Vitor Kaszuba | [JVKaszuba](https://gitlab.com/JVKaszuba) | [joaokaszuba@alunos.utfpr.edu.br](mailto:joaokaszuba@alunos.utfpr.edu.br)
|  ![](https://secure.gravatar.com/avatar/cec1634990143d9be0fd4c5783112007?s=800&d=identicon)  | Pedro Henrique Capelli Peruzzo | [PedroPeruzzo](https://gitlab.com/pedroperuzzo) | [pedroperuzzo@alunos.utfpr.edu.br](mailto:pedroperuzzo@alunos.utfpr.edu.br)
|  ![](https://gitlab.com/uploads/-/system/user/avatar/9168495/avatar.png?width=400)  | Pedro Vinicius Yamamoto Agner de Faria | [Yamzik](https://gitlab.com/yamzik) | [pedroviniciusyama@gmail.com](mailto:pedroviniciusyama@gmail.com)
